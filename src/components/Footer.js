import React from 'react'

import codeberg from '../assets/images/codeberg.png';

const Footer = props => (
  <footer id="footer">
    <section>
      <h2>Still under development.</h2>
      <p>
        Home Accountant is still under development. Do you have any new
        features or ideas in mind? Do no hesitate to contact me on my email,
        or drop some issues in the Codeberg repository.
      </p>
      <ul className="actions">
        <li>
          <a href="https://codeberg.org/joristruong/homeaccountant-app" target="_blank" rel="noreferrer" className="button">
            Go to Codeberg <img src={codeberg} style={{width: '35px', verticalAlign: '-8px', paddingLeft: 8}} alt="codeberg" />
          </a>
        </li>
      </ul>
    </section>
    <section>
      <h2>Joris Truong</h2>
      <dl className="alt">
        <dt>Website</dt>
        <dd>
        <a href="https://joristruong.codeberg.page/">Joris Truong</a>
        </dd>
        <dt>Email</dt>
        <dd>
          <a href="mailto:joris.truong@protonmail.com">joris.truong@protonmail.com</a>
        </dd>
      </dl>
      <ul className="icons">
        <li>
          <a href="https://www.linkedin.com/in/joris-truong-35a383131/" className="icon fa-linkedin alt">
          </a>
        </li>
      </ul>
    </section>
    <p className="copyright">
      &copy; Joris Truong. Design: <a href="https://html5up.net">HTML5 UP</a>.
    </p>
  </footer>
)

export default Footer
