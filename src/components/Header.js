import React from 'react'

import logo from '../assets/images/logo.svg';

const Header = (props) => (
    <header id="header" className="alt">
        <span className="logo"><img src={logo} alt="logo" /></span>
        <h1>Home Accountant</h1>
        <p>A free open-source bank account management app.</p>
    </header>
)

export default Header
