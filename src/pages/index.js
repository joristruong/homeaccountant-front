import { useStaticQuery, graphql } from "gatsby"
import React from 'react'
import Helmet from 'react-helmet'
import { Waypoint } from 'react-waypoint'
import Slider from 'react-slick'
import pic01 from '../assets/images/pic01.jpg'
import Header from '../components/Header'
import Layout from '../components/layout'
import Nav from '../components/Nav'

import screen1 from '../assets/images/screen1.jpg'
import screen2 from '../assets/images/screen2.jpg'
import screen3 from '../assets/images/screen3.jpg'
import screen4 from '../assets/images/screen4.jpg'
import screen5 from '../assets/images/screen5.jpg'
import screen6 from '../assets/images/screen6.jpg'
import screen7 from '../assets/images/screen7.jpg'
import googlePlay from '../assets/images/google_play.svg'
import android from '../assets/images/android.svg'

const ApkButton = () => {
  const data = useStaticQuery(graphql`
    query MyQuery {
      file(relativePath: {eq: "app.apk"}) {
        publicURL
        name
      }
    }
  `)
  return <a href={data.file.publicURL} className="button special">
    APK
    <img src={android} style={{width: '30px', verticalAlign: '-5px', paddingLeft: 10}} alt="android" />
  </a>
}

class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stickyNav: false,
    }
  }

  _handleWaypointEnter = () => {
    this.setState(() => ({ stickyNav: false }))
  }

  _handleWaypointLeave = () => {
    this.setState(() => ({ stickyNav: true }))
  }

  render() {
    return (
      <Layout>
        <Helmet title="Home Accountant" />

        <Header />

        <Waypoint
          onEnter={this._handleWaypointEnter}
          onLeave={this._handleWaypointLeave}
        ></Waypoint>
        <Nav sticky={this.state.stickyNav} />

        <div id="main">
          <section id="intro" className="main">
            <div className="spotlight">
              <div className="content">
                <header className="major">
                  <h2>An account management app</h2>
                </header>
                <p>
                  Home Accountant is built to manage bank accounts. 
                  It lets you create accounts, add transactions, 
                  organize them into categories and subcategories. 
                  It also provides various kinds of charts and graphs 
                  to help having a better understanding of your expenses.
                </p>
              </div>
              <span className="image">
                <img src={pic01} alt="coin" />
              </span>
            </div>
          </section>

          <section id="first" className="main special">
            <header className="major">
              <h2>Components</h2>
            </header>
            <ul className="features">
              <li>
                <span className="icon major style1 fa-user"></span>
                <h3>Transactions</h3>
                <p>
                  Register your transactions and browse them by day, month, or year.
                </p>
              </li>
              <li>
                <span className="icon major style3 fa-exchange"></span>
                <h3>Accounts</h3>
                <p>
                  Facilitate your management by creating separate accounts.
                </p>
              </li>
              <li>
                <span className="icon major style5 fa-sitemap"></span>
                <h3>Categories</h3>
                <p>
                  Organize your transactions into categories and get an overview of your finances.
                </p>
              </li>
            </ul>
          </section>

          <section id="second" className="main special">
            <header className="major">
              <h2>Screenshots</h2>
            </header>
            
            <div style={{paddingBottom: 50, margin: '0 auto', width: '80%'}}>
              <Slider {...{
                  infinite: false,
                  dots: true,
                }}>
                <div>
                  <img src={screen1} width="197" alt="screen1" onDoubleClick={() => window.open(screen1, "_blank")} />
                </div>
                <div>
                  <img src={screen2} width="197" alt="screen2" onDoubleClick={() => window.open(screen2, "_blank")} />
                </div>
                <div>
                  <img src={screen3} width="197" alt="screen3" onDoubleClick={() => window.open(screen3, "_blank")} />
                </div>
                <div>
                  <img src={screen4} width="197" alt="screen4" onDoubleClick={() => window.open(screen4, "_blank")} />
                </div>
                <div>
                  <img src={screen5} width="197" alt="screen5" onDoubleClick={() => window.open(screen5, "_blank")} />
                </div>
                <div>
                  <img src={screen6} width="197" alt="screen6" onDoubleClick={() => window.open(screen6, "_blank")} />
                </div>
                <div>
                  <img src={screen7} width="197" alt="screen7" onDoubleClick={() => window.open(screen7, "_blank")} />
                </div>
              </Slider>
            </div>
          </section>

          <section id="cta" className="main special">
            <header className="major">
              <h2>Download</h2>
              <p>
                Currently, the app is on the Play Store.
                <br />
                You can also download the APK below.
              </p>
            </header>
            <footer className="major">
              <ul className="actions">
                <li>
                  <a href="https://play.google.com/store/apps/details?id=com.joristruong.homeaccountantapp" target="_blank" rel="noreferrer" className="button special">
                    Play Store
                    <img src={googlePlay} style={{width: '30px', verticalAlign: '-5px', paddingLeft: 10}} alt="play store" />
                  </a>
                </li>
                <li>
                <ApkButton />
                </li>
              </ul>
            </footer>
          </section>
        </div>
      </Layout>
    )
  }
}

export default Index
