# Home Accountant Front

This is the front-end website for [Home Accountant](https://codeberg.org/joristruong/homeaccountant-app)

## Installation

Run `gatsby develop` in the terminal to start the dev site after cloning the repo.
